from ast import List


class Solution:
    def garbageCollection(self, garbage: List[str], travel: List[int]) -> int:

        def last_house_index(garbage: list[str], garbage_type: str):
            for i in range(len(garbage) - 1, - 1, - 1):
                if garbage_type in garbage[i]:
                    return i
            return  0

        def count_garbage_collection_time(garbage: list[str]):

            return len(''.join(garbage))

        def travel_times(garbage: list[str], travel: list[int]):
            garbage_types = ['M', 'P', 'G']
            travel_time = 0
            i = 0
            while i < len(garbage_types):
                travel_index = 0
                end_house = last_house_index(garbage, garbage_type=garbage_types[i])
                for _ in range(end_house):
                    travel_time += travel[travel_index]
                    travel_index += 1
                i += 1    
            return travel_time    


        return travel_times(garbage, travel) + count_garbage_collection_time(garbage)