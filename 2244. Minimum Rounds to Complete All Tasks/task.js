/**
 * @param {number[]} tasks
 * @return {number}
 */
var minimumRounds = function(tasks) {

    function count_occurences(arr){
        count = {}

        for (let i=0; i < arr.length; i++){
            
            if (arr[i] in count){
                count[arr[i]] += 1
            }
            else{
                count[arr[i]] = 1
            }
        }

        return count
    }

    let nums_no_rep = count_occurences(tasks)
    let ans = 0
    for (key in nums_no_rep){
        if (nums_no_rep[key] == 1){
            return  - 1
        }

        ans += Math.ceil(nums_no_rep[key] / 3)

    }

    return ans
    
};