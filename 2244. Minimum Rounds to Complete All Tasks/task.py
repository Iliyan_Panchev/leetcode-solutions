from ast import List
from collections import Counter
import math


class Solution:
    def minimumRounds(self, tasks: List[int]) -> int:

        num_no_rep = Counter(tasks)
        res = 0
        for _, count in num_no_rep.items():
            if count == 1:
                return -1
            res += math.ceil(count / 3)   
                

        return res