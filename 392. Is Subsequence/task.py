class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        if s == "":
            return True
        index = 0
        for i, v in enumerate(t):
            if s[index] == v:
                index += 1
            if index == len(s):  
                return True
        return False