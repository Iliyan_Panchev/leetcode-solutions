/**
 * @param {number[][]} grid
 * @return {number}
 */
var findChampion = function(grid) {
    
    var sumArr = function(arr){
        var s = 0;
        arr.forEach(num => {
            s += num
        })

        return s
    }


    var maxSum = 0;
    var result = 0;

    for (let i=0; i < grid.length; i++){
        
        currentSum = sumArr(grid[i])

        if (currentSum > maxSum) {
            maxSum = currentSum
            result = i
        }
    }

    return result
};