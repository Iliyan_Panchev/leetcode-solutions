from ast import List

class Solution:
    def findChampion(self, grid: List[List[int]]) -> int:

        arr = sorted({i: sum(v) for i, v in enumerate(grid)}.items(), key=lambda x: x[1])
        
        return arr[-1][0]
            

       

        