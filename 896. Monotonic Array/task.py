class Solution:
    def isMonotonic(self, nums: list[int]) -> bool:
        monotonic = []
        r_monotonic = []

        for i in range(len(nums)-1):
            if nums[i] <= nums[i+1]:
                monotonic.append(True)
            else:
                monotonic.append(False)    

        for i in range(len(nums) - 1, 0, -1):
            if nums[i] <= nums[i-1]:
                r_monotonic.append(True)
            else:
                r_monotonic.append(False)


        return all(monotonic) or all(r_monotonic)    