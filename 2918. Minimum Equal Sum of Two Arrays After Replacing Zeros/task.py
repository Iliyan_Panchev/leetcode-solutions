from ast import List


class Solution:
    def minSum(self, nums1: List[int], nums2: List[int]) -> int:

        def replace_zeros(arr: list):

            return [n if n != 0 else 1 for n in arr]
        
        replaced_nums1 = replace_zeros(nums1)
        replaced_nums2 = replace_zeros(nums2)

        s1 = sum(replaced_nums1)
        s2 = sum(replaced_nums2)

        if 0 not in nums1 and s1 < s2 or 0 not in nums2 and s2 < s1:
            return -1


        return s1 if s1 > s2 else s2