/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
var minSum = function(nums1, nums2) {
    
    function replaceZeroes(arr) {
        return arr.map(n => (n !== 0 ? n : 1));
    }

    function sumArr(arr){
        sum = 0;
        for (let i=0;i < arr.length; i++){

            sum += arr[i]
        }
        return sum
    }
    
    var nums1NoZeroes = replaceZeroes(nums1)
    var nums2NoZeroes = replaceZeroes(nums2)

    var sumOfNums1 = sumArr(nums1NoZeroes)
    var sumOfNums2 = sumArr(nums2NoZeroes)


    if (!nums1.includes(0) && sumOfNums1 < sumOfNums2 || !nums2.includes(0) && sumOfNums2 < sumOfNums1){
        return -1
    }

    return sumOfNums1 > sumOfNums2 ? sumOfNums1 : sumOfNums2


};