from ast import List

class Solution:
    def findPoisonedDuration(self, timeSeries: List[int], duration: int) -> int:
        reset_counter = 0

        for i in range(len(timeSeries) - 1):
            diff = abs(timeSeries[i] - timeSeries[i + 1])
            if diff < duration:
                reset_counter += (duration - diff)


        return len(timeSeries) * duration - reset_counter