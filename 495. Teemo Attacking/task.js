/**
 * @param {number[]} timeSeries
 * @param {number} duration
 * @return {number}
 */
var findPoisonedDuration = function(timeSeries, duration) {
    var resetCounter = 0

    for (let i=0; i < timeSeries.length - 1; i++){
        var diff = Math.abs(timeSeries[i] - timeSeries[i + 1])

        if (diff < duration){
            resetCounter += duration - diff
        }
    }
    return timeSeries.length * duration - resetCounter
};  
