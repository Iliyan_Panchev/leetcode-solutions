# Definition for a binary tree node.
from ast import List
from collections import Counter
from typing import Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

        
class Solution:
    def findMode(self, root: Optional[TreeNode]) -> List[int]:
        
        def in_order(node: TreeNode , arr: list=[]):
            if node is None:
                return
            in_order(node.left)
            arr.append(node.val)
            in_order(node.right)

            return arr    


        arr = Counter(in_order(root)).most_common()

        x = 0
        res = []
        for i in range(len(arr)):
            if i == 0:
                x = arr[i][1]
            if arr[i][1] == x:
                res.append(arr[i][0])

        return res        