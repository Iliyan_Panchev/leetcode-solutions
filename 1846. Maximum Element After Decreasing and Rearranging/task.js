/**
 * @param {number[]} arr
 * @return {number}
 */
var maximumElementAfterDecrementingAndRearranging = function(arr) {
    
    arr.sort((a, b) => a - b);
    arr[0] = 1

    var current = arr[0]

    for (let i=1; i < arr.length; i++){

        if (Math.abs(arr[i] - current) > 1){
            arr[i] = current + 1
        }
        
        current = arr[i]
    }

    return arr.pop()
};
