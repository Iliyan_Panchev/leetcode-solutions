from ast import List


class Solution:
    def maximumElementAfterDecrementingAndRearranging(self, arr: List[int]) -> int:

        if len(arr) == 1:
            return 1
        arr.sort()
        arr[0] = 1
        current = arr[0]
        for i in range(len(arr)):
            if abs(arr[i] - current) > 1:
                arr[i] = current + 1

            current = arr[i]   

        return arr[-1]    
        