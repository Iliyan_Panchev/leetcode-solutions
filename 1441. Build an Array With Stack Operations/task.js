var buildArray = function(target, n) {
    var res = [];
    var stack = [];

    for (let num = 1; num <= n; num++) {
        if (target.includes(num)) {
            res.push('Push');
            stack.push(num);
        } else {
            res.push('Push');
            res.push('Pop');
        }

        if (JSON.stringify(stack) == JSON.stringify(target)) {
            return res;
        }
    }

    return [];
};

