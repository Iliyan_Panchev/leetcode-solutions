from ast import List


class Solution:
    def buildArray(self, target: List[int], n: int) -> List[str]:
        res = []

        PUSH = 'Push'
        POP = 'Pop'
        stack = []

        for num in range(1, n + 1):
            if num in target:
                res.append(PUSH)
                stack.append(num)
            else:
                res.append(PUSH)
                res.append(POP)

            if stack == target:
                return res    
        
    
        
      