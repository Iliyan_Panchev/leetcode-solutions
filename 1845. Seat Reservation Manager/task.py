import heapq


class SeatManager:

    def __init__(self, n: int):
        self.seats = (list(range(1, n + 1)))
          

    def reserve(self) -> int:
        return heapq.heappop(self.seats)



    def unreserve(self, seatNumber: int) -> None:
        heapq.heappush(self.seats, seatNumber)

'''
Heaps are binary trees for which every parent node has a value less than or equal to any of its children.
This implementation uses arrays for which heap[k] <= heap[2*k+1] and heap[k] <= heap[2*k+2] for all k, 
counting elements from zero. For the sake of comparison, 
non-existing elements are considered to be infinite. 
The interesting property of a heap is that its smallest element is always the root, heap[0].
'''