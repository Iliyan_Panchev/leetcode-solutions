/**
 * @param {string[]} words
 * @return {number[]}
 */
var lastVisitedIntegers = function(words) {
    let numbers = [];
    let res = [];
    let index = 0;

    words.forEach(word => {
        if (word != 'prev'){
            numbers.push(Number(word))
            index = numbers.length - 1
        }
        else{
            if (numbers.length > 0 && index > -1){
                res.push(numbers[index]) 
                index -= 1
            }
            else{
                res.push(-1)
            }
        }
    });

    return res
};