class Solution:
    def lastVisitedIntegers(self, words: list[str]) -> list[int]:
        stck = []
        res = []
        index = 0
        for ch in words:
            if ch.isnumeric():
                stck.append(int(ch))
                index = len(stck) - 1
            else:
                res.append(stck[index] if index > -1  and stck else -1)   
                index -= 1

        return res        