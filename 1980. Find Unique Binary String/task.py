from ast import List


class Solution:
    def findDifferentBinaryString(self, nums: List[str]) -> str:
        def generate_strings(s: str='', res=[]):
            if len(s) == len(nums[0]):

                return s
            
            res.append(generate_strings(s + '0'))
            res.append(generate_strings(s + '1'))


            return res

        arr = generate_strings()


        for b in arr:
            if b not in nums and isinstance(b, str):
                return b