class Solution:
    def convert(self, s: str, numRows: int) -> str:
        if numRows == 1 or numRows >= len(s):
            return s
        # row, col = 0,0
        # stack = []
        # diagonal = False
        # res = ''

        def create_matrix(columns: int, rows: int) -> list[list[None]]:
            return [['' for _ in range(columns)] for _ in range(rows)]


        def out_of_matrix(maze: list[list[str | None]], point: tuple[int]):
            return not 0 <= point[0] < len(maze) or not 0 <= point[1] < len(maze[point[0]])


        mat = create_matrix(len(s) // 2 + 1, numRows)

        def driver_code(mat, s, row=0, col=0, stack=[], diagonal=False, res=''):
            while s:
                if out_of_matrix(mat, (row, col)): # check if we reached the end of the matrix
                    if stack:
                        row, col = stack.pop()  # get the last coordinate before reaching out of the matrix
                    if not diagonal: # check if diagonal route is chosen if matrix end is reached
                        diagonal = True # if it's not set it to true, because we've reached the bottom now we must go sideways diagonally
                        row, col = len(mat) - 1, col # set the coordinates to bottom
                        col += 1 # updated coordinates to the next so we don't skip letters
                        row -= 1
                    else:
                        diagonal = False # when the diagonal has reached the top, we check again if we are at matrix end and if we are we set the course to bottom again
                        row += 1
                mat[row][col] = s[0] # swap the empty slots in the matrix with the letter 
                s = s[1:] # remove the used letter and update the string with slicing
                stack.append((row, col)) # keep track of visited coordinates
                if not diagonal: # check if we are moving diagonally or not, if not move down
                    row += 1
                else:
                    row -= 1 # we are moving diagonally if diagonal is set to true
                    col += 1
            

            for i in range(len(mat)):
                res += ''.join(mat[i])

            return res            
        
        return driver_code(mat, s) 