class MyHashMap:

    def __init__(self):
        self.keys = []
        
    def put(self, key: int, value: int) -> None:
        for entry in self.keys:
            if entry[0] == key:
                entry[1] = value
                return 
        else:        
            self.keys.append([key, value])

    def get(self, key: int) -> int:
        for k, v in self.keys:
            if k == key:
                return v
        return -1    
        

    def remove(self, key: int) -> None:
        for i, entry in enumerate(self.keys):
            if entry[0] == key:
                self.keys.pop(i)