/**
 * @param {number[]} nums
 * @param {number} n
 * @return {number[]}
 */
var shuffle = function(nums, n) {

    let first = [];
    let second = [];
    let result = [];
    for (let i = 0; i < nums.length; i++){
        if (i < n){
            first.push(nums[i])
        }
        else{
            second.push(nums[i])
        }
    }

    for (let i = 0; i < n; i++){
        result.push(first[i])
        result.push(second[i])
    }


    return result
};  



