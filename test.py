nums = [1,3,5,3]



for n in set(nums):
    left = n - 1
    right = n + 1

    if left not in nums and right not in nums and nums.count(n) < 2:
        print(n)