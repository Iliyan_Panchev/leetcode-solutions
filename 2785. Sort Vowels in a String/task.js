/**
 * @param {string} s
 * @return {string}
 */
var sortVowels = function(s) {

    s = s.split('')
    
    const VOWELS = 'aeiouAEIOU';
    let stack = [];


    for (let i=0; i < s.length; i++){
        if (VOWELS.includes(s[i])){
            stack.push(s[i])
            s[i] = '*'
        }
    }


    stack.sort(function(a, b) {
        return b.charCodeAt(0) - a.charCodeAt(0);
    });

    for (let i=0; i < s.length; i++){
        if (s[i] == '*'){
            s[i] = stack.pop()
        }
    }


    return s.join('')
};