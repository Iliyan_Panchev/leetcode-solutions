class Solution:
    def sortVowels(self, s: str) -> str:
        s = list(s)
        vowels = 'aeiouAEIOU'
        arr = []
        for i, v in enumerate(s):
            if v in vowels:
                arr.append(v)
                s[i] = '*'
                
        if arr:
            if sorted(arr, reverse=True) == arr:
                return s
            else:
                arr.sort(reverse=True)
                
            for i, v in enumerate(s):
                if v == '*':
                    s[i] = arr.pop()

        return ''.join(s)
           