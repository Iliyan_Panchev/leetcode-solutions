from collections import Counter


class Solution:
    def majorityElement(self, nums: list[int]) -> list[int]:
        n = len(nums) / 3
        res = []
        d = Counter(nums)

        for k, v in d.items():
            if v > n:
                res.append(k)

        return res



#bonus one-liner
class Solution:
    def majorityElement(self, nums: list[int]) -> list[int]:

        return [n[0] for n in Counter(nums).items() if n[1] > len(nums) / 3]