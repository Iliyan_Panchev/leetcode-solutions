class Solution:
    def makeSmallestPalindrome(self, s: str) -> str:
        s = list(s)
        i, j = 0, len(s) - 1
        while i < j:
            s[i] = min(s[i], s[j])
            s[j] = s[i]
            i += 1
            j -= 1
            if s == s[::-1]:
                return ''.join(s)
        
        return ''.join(s)