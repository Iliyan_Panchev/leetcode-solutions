class Solution:
    def backspaceCompare(self, s: str, t: str) -> bool:

        def remove_ch(arr: list[str]):
            arr = list(arr)
            backspace = '#'
            while backspace in arr:
                for i in range(len(arr)):
                    if arr[i] == backspace:
                        arr.pop(i)
                        if i < 1:
                            break
                        arr.pop(i-1)
                        break
            return arr

        return remove_ch(s) == remove_ch(t) 