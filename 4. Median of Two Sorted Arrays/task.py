class Solution:
    def findMedianSortedArrays(self, nums1: list[int], nums2: list[int]) -> float:
        nums3 = nums1 + nums2
        nums3.sort()
        if len(nums3) % 2 == 0:
            a = nums3[len(nums3) // 2]
            b = nums3[len(nums3) // 2 - 1]
            return (a + b) / 2
        
        return nums3[len(nums3) // 2]    