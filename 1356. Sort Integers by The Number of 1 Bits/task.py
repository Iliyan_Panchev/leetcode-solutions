from collections import defaultdict
from itertools import chain

class Solution:
    def sortByBits(self, arr: list[int]) -> list[int]:

        d = defaultdict(list)

        for i, v in enumerate(arr):
            d[bin(v)[2:].count('1')].append(v)

        d = {k: v for k, v in sorted(d.items(), key=lambda item: item[0])}

        
        [v.sort() for v in d.values()]

        return list(chain.from_iterable(d.values()))