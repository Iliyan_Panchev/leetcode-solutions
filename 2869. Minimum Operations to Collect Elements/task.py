class Solution:
    def minOperations(self, nums: list[int], k: int) -> int:
        collected = []

        range_ = [n for n in range(1, k+1)]

        for i in range(len(nums) - 1, - 1, - 1):
            if set(range_).issubset(set(collected)):
                return len(collected)
            else:
                collected.append(nums[i])

        return len(collected)        