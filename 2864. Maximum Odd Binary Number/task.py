class Solution:
    def maximumOddBinaryNumber(self, s: str) -> str:
        s = list(s)
        s.sort(reverse=True)

        for i, v in enumerate(s):
            if v == '0':
                s[i - 1] = '0'
                s[-1] = '1'
                break

        return ''.join(s)
    
#alterntive faster solution

class Solution:
    def maximumOddBinaryNumber(self, s: str) -> str:
        ones = s.count('1') - 1
        zeroes = s.count('0')

        return ('1' * ones) + ('0' * zeroes) + '1' 