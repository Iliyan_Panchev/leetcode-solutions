/**
 * @param {number[]} nums
 * @return {number}
 */
var minPairSum = function(nums) {
    
    let result = 0;
    let right = nums.length - 1;
    nums.sort(function(a, b) {return a-b})

    for (let left=0; left < nums.length; left++){

        if (left > right){
            break
        }

        let current = nums[left] + nums[right];

        if (current > result){
            result = current
        }

        right--

    }


    return result
};
