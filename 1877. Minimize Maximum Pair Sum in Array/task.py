from ast import List


class Solution:
    def minPairSum(self, nums: List[int]) -> int:

        nums.sort()
        res = 0
        j = len(nums) - 1
        for i in range(len(nums)):
            if i >= j:
                break
            current = nums[i] + nums[j]
            if current > res:
                res = current
                
            j -= 1


        return res   


        